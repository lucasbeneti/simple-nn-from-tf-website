import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

# setando e baixando as imagens pra serem usadas como treinamento e teste
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels),(test_images,test_labels) = fashion_mnist.load_data()
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# preprocessing pra entrada da rede ser um número entre 0 e 1
# dividindo por 255.0 como float pra ser 0 e 1
train_images = train_images/255.0
test_images = test_images/255.0

# só um plot de imagens (não serve pra muita coisa)
# plt.figure(figsize=(10,10))
# for i in range(25):
#     plt.subplot(5,5,i+1)
#     plt.xticks([])
#     plt.yticks([])
#     plt.grid(False)
#     plt.imshow(train_images[i], cmap=plt.cm.binary)
#     plt.xlabel(class_names[train_labels[i]])
# plt.show()

# estrutura de layers da NN
# layer Flatten é pra fazer um reshape na entrada, que passa de uma matriz 2D
# pra um array com as linhas ordenadas num array
# as outras duas Dense, a primeira é só
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28,28)),
    keras.layers.Dense(128, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax)
])

# argumentos para compilar o modelo
model.compile(optimizer='adam',
                loss='sparse_categorical_crossentropy',
                metrics=['accuracy'])

model.fit(train_images, train_labels, epochs=5)

# avaliar a precisão do modelo treinado acima (ignorando a merda por teste)
_, test_acc = model.evaluate(test_images, test_labels)
print("Test acc: ", test_acc)

# faz a previsão e verifica com o label da mesma imagem
predictions = model.predict(test_images)
# print(np.argmax(predictions[144]))
# print(test_labels[144])

def plot_image(i, predictions_array, true_label, img):
  predictions_array, true_label, img = predictions_array[i], true_label[i], img[i]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])
  
  plt.imshow(img, cmap=plt.cm.binary)

  predicted_label = np.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'blue'
  else:
    color = 'red'
  
  plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                100*np.max(predictions_array),
                                class_names[true_label]),
                                color=color)

def plot_value_array(i, predictions_array, true_label):
  predictions_array, true_label = predictions_array[i], true_label[i]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])
  thisplot = plt.bar(range(10), predictions_array, color="#777777")
  plt.ylim([0, 1]) 
  predicted_label = np.argmax(predictions_array)
 
  thisplot[predicted_label].set_color('red')
  thisplot[true_label].set_color('blue')

# i = 144
# plt.figure(figsize=(6,3))
# plt.subplot(1,2,1)
# plot_image(i, predictions, test_labels, test_images)
# plt.subplot(1,2,2)
# plot_value_array(i, predictions,  test_labels)
# plt.show()

# selecionada uma imagem das de teste
img_quero = test_images[4999]
print(img_quero.shape)

# tf.keras é otimizado pra fazer previsões de uam batch de imagens, por isso
# tem que adicionar pra uma lista, nesse caso, adicionando em uma batch onde
# ela seja a única imagem
img_quero = (np.expand_dims(img_quero,0))
print(img_quero.shape)

predictions_single = model.predict(img_quero)
print(predictions_single)
print(np.argmax(predictions_single[0]))
print(test_labels[4999])
